# OpenML dataset: djdc0093

https://www.openml.org/d/496

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Dow-Jones Industrial Average (DJIA) closing values from 1900 to 1993.
First column contains the date (yymmdd), second column contains the value.
These data are used in: E.Ley (1996): "On the Peculiar Distribution of the
U.S. Stock Indices;" forthcoming in  The American Statistician.
---


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: last

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/496) of an [OpenML dataset](https://www.openml.org/d/496). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/496/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/496/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/496/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

